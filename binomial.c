#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/// The number of stars the histogram visualization may have on a value of `1.0`.
#define HISTOGRAM_VIS_LENGTH  (200)

typedef struct conf {
    uint64_t generations;
    double probability;
    size_t binom_attempts;
} conf_t;

/// Parses the command line arguments and populates `out_conf` with them.
static int parse_args(conf_t* out_conf, int argc, char** argv) {
    if (argc != 4) {
        fprintf(stderr, "USAGE: %s <generations> <n> <p>\n", argv[0]);
        return 1;
    }

    out_conf->generations = strtoll(argv[1], NULL, 10);
    if (errno) {
        fprintf(stderr, "Invalid number of generations: %s\n", argv[1]);
        return 2;
    }

    out_conf->binom_attempts = strtoll(argv[2], NULL, 10);
    if (errno) {
        fprintf(stderr, "Invalid number of attempts (n): %s\n", argv[1]);
        return 4;
    }

    out_conf->probability = strtod(argv[3], NULL);
    if (errno) {
        fprintf(stderr, "Invalid probability (p): %s\n", argv[1]);
        return 3;
    }

    return 0;
}

/// Reseeds the `rand()` generator.
static inline void reseed_generator() {
    struct timespec tp;
    clock_gettime(CLOCK_MONOTONIC, &tp);
    srand(tp.tv_sec + tp.tv_nsec);
}

/// Gets a double value based on `rand()`
static inline double rand_double() {
    return (double) rand() / (double) RAND_MAX;
}

/// Generates `attempts` numbers and checks if they are <= `probability` - the number of successful attempts is
/// the result.
static size_t attempt_randomly(double probability, size_t attempts) {
    size_t result = 0;
    for (size_t i = 1; i <= attempts; i++) {
        if (rand_double() <= probability)  result++;
    }
    return result;
}

/// Generates a relative histogram that *should* be similar to a binomial distribution function.
static void gen_binomial_histogram(double* out_rel_histogram, uint64_t generations, double probability,
                                   size_t binom_attempts)
{
    uint64_t histogram[binom_attempts + 1];
    memset(histogram, 0, sizeof(histogram));

    for (uint64_t i = 0; i < generations; i++) {
        histogram[attempt_randomly(probability, binom_attempts)]++;
    }

    for (size_t i = 0; i <= binom_attempts; i++) {
        out_rel_histogram[i] = (double) histogram[i] / (double) generations;
    }
}

/// Calculates the mean of a relative histogram.
static double calc_mean(const double* rel_histogram, size_t binom_attempts) {
    double mean = 0;
    for (size_t i = 0; i <= binom_attempts; i++) {
        mean += (double) i * rel_histogram[i];
    }

    return mean;
}

/// Calculates the variance of a relative histogram.
static double calc_variance(const double* rel_histogram, size_t binom_attempts, double mean) {
    double variance = 0;
    for (size_t i = 0; i <= binom_attempts; i++) {
        double dif = (double) i - mean;
        variance += dif * dif * rel_histogram[i];
    }

    return variance;
}

/// Finds the `first` and `last` indices of nonzero values in `rel_histogram`.
static void analyze_nonzeroes(const double* rel_histogram, size_t binom_attempts, size_t* first, size_t* last) {
    size_t line = 0;
    for (; line <= binom_attempts; line++) {
        if (rel_histogram[line] != 0) {
            *first = line++;
            break;
        }
    }
    for (; line <= binom_attempts; line++) {
        if (rel_histogram[line] != 0) {
            *last = line;
        }
    }
}


/// The main program.
void run(conf_t conf) {
    reseed_generator();

    double rel_histogram[conf.binom_attempts + 1];
    gen_binomial_histogram(rel_histogram, conf.generations, conf.probability, conf.binom_attempts);

    double theory_mean = (double) conf.binom_attempts * conf.probability;
    double theory_variance = (double) conf.binom_attempts * conf.probability * (1 - conf.probability);
    double actual_mean = calc_mean(rel_histogram, conf.binom_attempts);
    double actual_variance = calc_variance(rel_histogram, conf.binom_attempts, actual_mean);

    printf("E_teorie=%lf\n", theory_mean);
    printf("D_teorie=%lf\n", theory_variance);
    printf("E_vypocet=%lf\n", actual_mean);
    printf("D_vypocet=%lf\n", actual_variance);

    putchar('\n');

    size_t first_nonzero_value = 0;
    size_t last_nonzero_value = conf.binom_attempts;
    analyze_nonzeroes(rel_histogram, conf.binom_attempts, &first_nonzero_value, &last_nonzero_value);

    for (size_t line = first_nonzero_value; line <= last_nonzero_value; line++) {
        printf("%8zu: ", line);
        uint64_t stars = (uint64_t) (rel_histogram[line] * HISTOGRAM_VIS_LENGTH);
        for (uint64_t i = 0; i < stars; i++) putchar('*');
        putchar('\n');
    }

    putchar('\n');
}


/// Program entry point.
int main(int argc, char** argv) {
    if (argc == 1) {
        conf_t c1 = {
                .generations = 1000000,
                .probability = 0.5,
                .binom_attempts = 50
        };
        run(c1);

        conf_t c2 = {
                .generations = 100000,
                .probability = 0.2,
                .binom_attempts = 80
        };
        run(c2);
    } else {
        conf_t conf;
        if (parse_args(&conf, argc, argv))  return 1;
        run(conf);
    }

    return 0;
}
